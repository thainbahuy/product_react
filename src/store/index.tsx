import reducer from './../reducers';
import { createStore, compose, applyMiddleware } from 'redux';
import { composeWithDevTools } from "redux-devtools-extension";
import logger from 'redux-logger'
import thunk from 'redux-thunk'

// const composeEnhancers =  compose;
//@ts-ignore
const store = createStore(reducer, compose(applyMiddleware(thunk), applyMiddleware(logger)),composeWithDevTools());
export default store;
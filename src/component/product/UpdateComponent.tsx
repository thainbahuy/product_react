import React, { Component } from 'react'
import client from '../../client'
import { async } from 'q'

interface Product {
    Id: string,
    Name: string,
    Title: string,
    Price: number,
}

interface IProps {
    createProductEvent: (product: Product) => void,
    match: any,
    history: any
}

interface IState {
    product: {
        Id: string,
        Name: string,
        Title: string,
        Price: number,
    }
}
class UpdateComponent extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = {
            product: {
                Id: '',
                Name: '',
                Title: '',
                Price: 0,
            }
        }
    }
    handleChangeName = (e: any) => {
        this.setState({
            product: {
                Id: this.state.product.Id,
                Name: e.target.value,
                Title: this.state.product.Title,
                Price: this.state.product.Price,
            }
        })
    }

    handleChangeTitle = (e: any) => {
        this.setState({
            product: {
                Id: this.state.product.Id,
                Name: this.state.product.Name,
                Title: e.target.value,
                Price: this.state.product.Price,
            }
        })
    }

    handleChangePrice = (e: any) => {
        this.setState({
            product: {
                Id: this.state.product.Id,
                Name: this.state.product.Name,
                Title: this.state.product.Title,
                Price: e.target.value,
            }
        })
    }

    componentWillMount = () => {
        this.getProductById(this.props.match.params.id);
    }

    updateProduct = async (e: any) => {
        e.preventDefault();
        let result = await client.put(`api/products/update/${this.state.product.Id}`, {
            name: this.state.product.Name,
            title: this.state.product.Title,
            price: this.state.product.Price,
        })
        this.props.history.push(`/`);
    }

    getProductById = async (id: any) => {
        var result = await client.get(`api/products/edit/${id}`);
        this.setState({
            product: {
                Id: result.data.result.Id,
                Name: result.data.result.Name,
                Title: result.data.result.Title,
                Price: result.data.result.Price,
            }
        })
    }

    render() {
        return (
            <div className="p-5">
                <form onSubmit={this.updateProduct}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input value={this.state.product.Name} onChange={this.handleChangeName} type="text" className="form-control" id="name"></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body">Title:</label>
                        <input value={this.state.product.Title} onChange={this.handleChangeTitle} type="text" className="form-control" id="title"></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body">Price:</label>
                        <input value={this.state.product.Price} onChange={this.handleChangePrice} type="text" className="form-control" id="price"></input>
                    </div>
                    <button type="submit" className="btn btn-primary">Save</button>
                </form>
            </div>
        )
    }
}

export default UpdateComponent;
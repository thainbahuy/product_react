import React from 'react'
import client from '../../client'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createProduct } from './../../actions'

interface Product {
    Id: string,
    Name: string,
    Title: string,
    Price: number,
}
interface IProps {
    
}
interface IState {
    product: {
        Id: string,
        Name: string,
        Title: string,
        Price: number,
    }
}
class CreateComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            product: {
                Id: '',
                Name: '',
                Title: '',
                Price: 0,
            }
        }
    }
    //handle change param
    handleChangeName = (e: any) => {
        this.setState({
            product: {
                Id: this.state.product.Id,
                Name: e.target.value,
                Title: this.state.product.Title,
                Price: this.state.product.Price,
            }
        })
    }

    handleChangeTitle = (e: any) => {
        this.setState({
            product: {
                Id: this.state.product.Id,
                Name: this.state.product.Name,
                Title: e.target.value,
                Price: this.state.product.Price,
            }
        })
    }

    handleChangePrice = (e: any) => {
        this.setState({
            product: {
                Id: this.state.product.Id,
                Name: this.state.product.Name,
                Title: this.state.product.Title,
                Price: e.target.value,
            }
        })
    }

    handleSubmit = async (e: any) => {
        e.preventDefault();
        //@ts-ignore
        this.props.createProduct(this.state.product);
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input onChange={this.handleChangeName} value={this.state.product.Name} type="text" className="form-control" id="title"></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body">Title:</label>
                        <input onChange={this.handleChangeTitle} value={this.state.product.Title} type="text" className="form-control" id="body"></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body">Price:</label>
                        <input onChange={this.handleChangePrice} value={this.state.product.Price} type="number" className="form-control" id="body"></input>
                    </div>
                    <button type="submit" className="btn btn-primary">Add new</button>
                </form>
            </div>
        )
    }
}

//hàm kết nối đến các actions để dispatch thong qua prop cua component
const mapDispatchToProps = (dispatch: any) => bindActionCreators({ createProduct }, dispatch)

//connect() dùng để kết nối đến store để giao tiếp các actions,reducer,store rồi trả về view
export default connect(null, mapDispatchToProps)(CreateComponent);
import React from 'react';
import CreateComponent from './CreateComponent'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchProduct, deleteProduct } from './../../actions'

type ProductTypes = {
  Id: string,
  Name: string,
  Title: string,
  Price: number,
}

interface IinitState {
  products: ProductTypes[]
}
class ListComponent extends React.Component {
  state: IinitState;
  constructor(props) {
    super(props);
    this.state = {
      products: [] //chua su dung
    }
  }

  componentDidMount() {
    //@ts-ignore
    this.props.fetchProduct();
  }

  handleCreateProduct = () => {

  }

  renderRows = (products) => {
    return products.map((item, index) => {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{item.Name}</td>
          <td>{item.Title}</td>
          <td>{item.Price}</td>
          <td>
            <button onClick={() => {
              this.onHandleDelete(item.Id,index)
            }} type="button" className="btn btn-danger">Delete</button>
            <Link to={`/update/${item.Id}`}>
              <button className="btn btn-primary">Update</button>
            </Link>
          </td>
        </tr>
      );
    })
  }

  onHandleDelete = (id,index) => {
    //@ts-ignore
    this.props.deleteProduct(id,index);
  }
  
  render() {
    //@ts-ignore
    let products = this.props.products
    return (
      <div className='p-5'>
        <h1>Products</h1>
        <div className="row">
          <div className="col-md-5">
            <CreateComponent />
          </div>
        </div><br />
        <table className="table table-dark table-striped">
          <thead>
            <tr>
              <td>ID</td>
              <td>Name</td>
              <td>Title</td>
              <td>Price</td>
              <td style={{ width: '200px' }} >Actions</td>
            </tr>
          </thead>
          <tbody>
            {this.renderRows(products)}
          </tbody>
        </table>
      </div>
    );
  }
}

//hàm đăng kí đến store chứa các state, để nhận update state khi store cap nhat
const mapStateToProps = (state) => {
  const { productReducer } = state;
  return {
    products: productReducer.products
  }
}

//hàm kết nối đến các actions để dispatch thong qua prop cua component
const mapDispatchToProps = (dispatch: any) => bindActionCreators({ fetchProduct, deleteProduct }, dispatch)

//connect() dùng để kết nối đến store để giao tiếp các actions,reducer,store rồi trả về view
export default connect(mapStateToProps, mapDispatchToProps)(ListComponent);
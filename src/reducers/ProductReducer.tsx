import * as type from '../constants/ActionTypes';

const initState = {
    products: []
}

const productReducer = (state = initState, action: any) => {
    switch (action.type) {
        case type.GET_LIST_PRODUCTS_SUCCESS:
            
            return { ...state, products: action.payload.listProduct }

        case type.GET_LIST_PRODUCTS_FAILURE:
           
            return { ...state };

        case type.DELETE_PRODUCT_SUCCESS:
            let { products } = state;
            // @ts-ignore
            const temp = products.filter(item => item.Id !== action.payload.id )
            return { products: temp };

        case type.DELETE_PRODUCT_FAILURE: 
            return { ...state };

        case type.CREATE_PRODUCT_SUCCESS:
            return { ...state, products: [action.payload.product, ...state.products] }
            
        
        case type.CREATE_PRODUCT_FAILURE:
           
            return { ...state };

        default: return { ...state };
    }
}

export default productReducer;
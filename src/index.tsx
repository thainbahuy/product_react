import * as React from "react";
import ReactDOM from "react-dom";
import {
    BrowserRouter,
    Route, Switch
} from "react-router-dom";
import UpdateProductComponent from './component/product/UpdateComponent'
import ListComponent from './component/product/ListComponent';
import store  from './store/index';
import { Provider } from 'react-redux'

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/update/:id" component={UpdateProductComponent} />
                <Route exact path="/" component={ListComponent} />
            </Switch>
        </BrowserRouter>
        </Provider>,
    document.getElementById('root'));
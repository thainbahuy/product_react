import axios from 'axios';

var axiosInstance = axios.create({
  baseURL: 'http://localhost:11400',
  /* other custom settings */
});

export default axiosInstance;
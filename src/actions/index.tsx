import * as type from '../constants/ActionTypes';
import client from './../client';

export const fetchProduct = () => {
    return dispatch => {
        return client.get("/api/products/getListProduct").then(res => {
            if (res.data.listProduct.length > 0) {
                dispatch(getListProductSuccess(res.data))
            } else {
                dispatch(getListProductFailure())
            }

        })
    }
}

export const getListProductSuccess = (data) => {
    return {
        type: type.GET_LIST_PRODUCTS_SUCCESS,
        payload: data
    }
}

export const getListProductFailure = () => {
    return {
        type: type.GET_LIST_PRODUCTS_FAILURE,
    }
}

export const deleteProduct = (product_id, index) => {
    return dispatch => {
        return client.delete(`/api/products/${product_id}`).then(res => {
            if (res.status === 200) {
                dispatch(deleteProductSuccess(product_id));
            } else {
                dispatch(deleteProductFailure());
            }
        })
    }
}

export const deleteProductSuccess = (id) => {
    return {
        type: type.DELETE_PRODUCT_SUCCESS,
        payload: {
            id,
        }
    }
}

export const deleteProductFailure = () => {
    return {
        type: type.DELETE_PRODUCT_FAILURE,
    }
}

export const createProduct = (product) => {
    return dispatch => {
        return client.post(`api/products/store`,
            {
                name: product.Name,
                title: product.Title,
                price: product.Price,

            }).then(res => {
                if (res.status === 200) {
                    product.Id = res.data.id;
                    dispatch(createProductSuccess(product));
                } else {
                    dispatch(createProductFailure());
                }
            })
    }
}

export const createProductSuccess = (product) => {
    return {
        type: type.CREATE_PRODUCT_SUCCESS,
        payload: {
            product: product,
        }
    }
}

export const createProductFailure = () => {
    return {
        type: type.CREATE_PRODUCT_FAILURE,
    }
}